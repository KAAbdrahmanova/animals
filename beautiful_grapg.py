from mindy.graph import basics as bas
from mindy.raph import config
from mindy.graph import basics as bg
from mindy.graph import neo4j_graph as pg

def create_graph():
    """Создаёт основу для будущего графа"""
    animals = pg.PersistentGraph(config.graph_connection, graph_segment="graph_by_K")
    #animals=animals.Match({}).Delete()
    #млекопитающие
    mammals=bas.Node(animals,{"animal_type":"млекопитающее","type":"млекопитающее"})
    home=bas.Node(animals,{"character":"домашнее","type":"домашнее"})
    mammals.Connect(home)
    cat=bas.Node(animals,{"answer":"да","is":"киса","type":"киса"})
    wolf=bas.Node(animals,{"answer":"нет","is":"волк","type":"волк"})
    home.Connect(cat)
    home.Connect(wolf)
    #насекомые
    insects=bas.Node(animals,{"animal_type":"насекомое","type":"насекомое"})
    fly=bas.Node(animals,{"character":"летает","type":"летает"})
    insects.Connect(fly)
    baterfly=bas.Node(animals,{"answer":"да","is":"бабочка","type":"бабочка"})
    ant=bas.Node(animals,{"answer":"нет","is":"муравей","type":"муравей"})
    fly.Connect(baterfly)
    fly.Connect(ant)
    return animals


def remember_animal(wrong_is,animals,yes_or_no):
    """Запоминаем новое животное"""
    correct_is=input("Ну вот(Кто же это?")
    new_characteristic=input("Чем же "+correct_is+" отличается от "+wrong_is+"?\n Он(она) ")
    
    parent=animals.MatchOne({'is':wrong_is}).ConnectedWith()[0]
    animals.MatchOne({'is':wrong_is}).Delete()
    new_Node_character=bas.Node(animals,{'answer':yes_or_no,'character':new_characteristic,'type':new_characteristic})
    no_is=bas.Node(animals,{'answer':'нет','is':wrong_is,'type':wrong_is})
    yes_is=bas.Node(animals,{'answer':'да','is':correct_is,'type':correct_is})
    new_Node_character.Connect(no_is)
    new_Node_character.Connect(yes_is)
    parent.Connect(new_Node_character)
    return animals

def answer(var):
    """Выбирает вариант, на который пользователь ответил да"""
    for v in var:
        if input(str(v)+"?")=="да":return v

def all_type_animal(animals):
    return [t["animal_type"] for t in animals.Match({}) if "animal_type" in t]

def game():
    animals=create_graph()
    types_animal=all_type_animal(animals)#формирует список узлов из типов всех животных

    while True:
        ans=answer(types_animal)
        while ans==None:
            if input("Выбирай между млекопитающими и насекомыми!\nНапиши ХОРОШО, если хочешь начать заново\nЕсли хочешь закончить введи НЕТ ")=='НЕТ':
                print("Пока")
                return
            else:
                types_animal=all_type_animal(animals)
                ans=answer(types_animal)
        child_answer=animals.MatchOne({"animal_type":ans}).Child({})
        while ("is" not in child_answer):
            ans=input(child_answer["character"]+'?')
            child_answer=child_answer.Child({"answer":ans})
        final_answer=input("Это "+child_answer["is"]+" ?")
        if final_answer=="да":print("Ура, я угадал!")
        else: animals=remember_animal(child_answer["is"],animals,ans)
        if(input("Хочешь продолжить?")=='нет'):
            #animals=animals.Match({}).Delete()
            print("Пока)")
            return

if __name__ == '__main__':
    game()